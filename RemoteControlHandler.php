<?php
/**
 * Handler for extendRemoteControl Plugin for LimeSurvey : Functions added here
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @author Florian Döring <f.doering@dkfz-heidelberg>
 * @copyright 2015-2016 Denis Chenu <http://sondages.pro>
 * @copyright 2022 Florian Döring <f.doering@dkfz-heidelberg>
 * @license GPL v3
 * @version 1.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class RemoteControlHandler extends remotecontrol_handle
{
    /**
     * @inheritdoc
     * Disable webroute else json returned can be broken
     */
    public function __construct(AdminController $controller)
    {
        /* Deactivate web log */
        foreach (Yii::app()->log->routes as $route) {
            $route->enabled = $route->enabled && !($route instanceOf CWebLogRoute);
        }
        parent::__construct($controller);
    }

    /**
    * RPC Routine to get information on user from extendRemoteControl plugin
    *
    * @access public
    * @param string $sSessionKey Auth credentials
    * @return array The information on user (except password)
    */
    public function get_me($sSessionKey)
    {
        if ($this->_checkSessionKey($sSessionKey))
        {
            $oUser=User::model()->find("uid=:uid",array(":uid"=>Yii::app()->session['loginID']));
            if($oUser) // We have surely one, else no sessionkey ....
            {
                $aReturn=$oUser->attributes;
                unset($aReturn['password']);
                $aReturn['loginID'] = Yii::app()->session['loginID'];
                return $aReturn;
            }
        }
    }

    /**
    * RPC Routine to get global permission of the actual user
    *
    * @access public
    * @param string $sSessionKey Auth credentials
    * @param string $sPermission string Name of the permission - see function getGlobalPermissions
    * @param $sCRUD string The permission detailsyou want to check on: 'create','read','update','delete','import' or 'export'
    * @return bool True if user has the permission
    * @return boolean
    */
    public function hasGlobalPermission($sSessionKey,$sPermission,$sCRUD='read')
    {
        $this->_checkSessionKey($sSessionKey);
        return array(
            'permission'=>Permission::model()->hasGlobalPermission($sPermission,$sCRUD)
        );
    }

    /**
    * RPC Routine to get survey permission of the actual user
    *
    * @access public
    * @param string $sSessionKey Auth credentials
    * @param $iSurveyID integer The survey ID
    * @param $sPermission string Name of the permission
    * @param $sCRUD string The permission detail you want to check on: 'create','read','update','delete','import' or 'export'
    * @return bool True if user has the permission
    * @return boolean
    */
    public function hasSurveyPermission($sSessionKey,$iSurveyID, $sPermission, $sCRUD='read')
    {
        $this->_checkSessionKey($sSessionKey);
        return array(
            'permission'=>\Permission::model()->hasSurveyPermission($iSurveyID, $sPermission, $sCRUD),
        );
    }

    /**
     * Add an empty survey with minimum details and set its owner to an existing user
     *
     * This just tries to create an empty survey with the minimal settings.
     *
     * Failure status: Invalid session key, No permission, Faulty parameters, Creation Failed result
     *
     * @access public
     * @param string $sSessionKey Auth credentials
     * @param string $sSurveyOwner Set the owner of the new survey to this user name
     * @param int $iSurveyID The desired ID of the Survey to add
     * @param string $sSurveyTitle Title of the new Survey
     * @param string $sSurveyLanguage Default language of the Survey
     * @param string $sformat (optional) Question appearance format (A, G or S) for "All on one page", "Group by Group", "Single questions", default to group by group (G)
     * @return int|array The survey id in case of success
     */
    public function add_survey_to_owner($sSessionKey, $sSurveyOwner, $iSurveyID, $sSurveyTitle, $sSurveyLanguage, $sformat = 'G')
    {
        $sSurveyOwner = (string) $sSurveyOwner;
        $iSurveyID = (int) $iSurveyID;
        $sSurveyTitle = (string) $sSurveyTitle;
        $sSurveyLanguage = (string) $sSurveyLanguage;
        Yii::app()->loadHelper("surveytranslator");
        if ($this->_checkSessionKey($sSessionKey)) {
            if (Permission::model()->hasGlobalPermission('surveys', 'create')) {
                if ($sSurveyOwner == '' || $sSurveyTitle == '' || $sSurveyLanguage == '' || !array_key_exists($sSurveyLanguage, getLanguageDataRestricted()) || !in_array($sformat, array('A', 'G', 'S'))) {
                                    return array('status' => 'Faulty parameters');
                }

                $oOwnerUser = User::model()->findByAttributes(['users_name' => $sSurveyOwner]);
                $aInsertData = array(
                    'template' => App()->getConfig('defaulttheme'),
                    'owner_id' => $oOwnerUser->uid,
                    'active' => 'N',
                    'language' => $sSurveyLanguage,
                    'format' => $sformat
                );

                if (!is_null($iSurveyID)) {
                    $aInsertData['wishSID'] = $iSurveyID;
                }

                try {
                    $newSurvey = Survey::model()->insertNewSurvey($aInsertData);
                    if (!$newSurvey->sid) {
                        return array('status' => 'Creation Failed'); // status are a string, another way to send errors ?
                    }
                    $iNewSurveyid = $newSurvey->sid;

                    $sTitle = html_entity_decode($sSurveyTitle, ENT_QUOTES, "UTF-8");

                    $aInsertData = array(
                        'surveyls_survey_id' => $iNewSurveyid,
                        'surveyls_title' => $sTitle,
                        'surveyls_language' => $sSurveyLanguage,
                    );

                    $langsettings = new SurveyLanguageSetting();
                    $langsettings->insertNewSurvey($aInsertData);
                    Permission::model()->giveAllSurveyPermissions($oOwnerUser->uid, $iNewSurveyid);

                    return (int) $iNewSurveyid;
                } catch (Exception $e) {
                    return array('status' => $e->getmessage());
                }
            } else {
                            return array('status' => 'No permission');
            }
        } else {
                    return array('status' => self::INVALID_SESSION_KEY);
        }
    }
}
